#include <stdio.h>

void update(int *a, int *b) {
	// Complete this function    
	int index;
	index = *a;
	*a = *a + *b;
	if (*b - index < 0)
	{
		*b = index - *b;
	}
	else {
		*b = *b - index;
	}
}

int main() {
	int a, b;
	int *pa = &a, *pb = &b;

	scanf("%d %d", &a, &b);
	update(pa, pb);
	printf("%d\n%d", a, b);

	return 0;
}