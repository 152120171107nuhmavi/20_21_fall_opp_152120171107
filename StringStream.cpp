#include <sstream>
#include <vector>
#include <iostream>
using namespace std;

vector<int> parseInts(string str) {

	vector<int> strtonumber;
	stringstream ss(str);
	char a;
	int b;

	ss >> b;
	strtonumber.push_back(b);

	for (int i = 0; i < str.size(); i++)
	{
		ss >> a >> b;
		if (ss.fail())
		{
			break;
		}
		else {
			strtonumber.push_back(b);
		}
	}

	return strtonumber;

}

int main() {
	string str;
	cin >> str;
	vector<int> integers = parseInts(str);
	for (int i = 0; i < integers.size(); i++) {
		cout << integers[i] << "\n";
	}

	return 0;
}